# Workshop Análise de redes e vínculos utiliando bases textuais

**  Material apresentado durante o Workshop "Análise de redes e vínculos
utilizando bases textuais" no I Congresso Internacional em Humanidades Digitais
no Rio de Janeiro - HDRio2018 **

### Hands on

```
git clone git@bitbucket.org:twistsystems/workshop_hdrio2018.git
```

### Instalação

```
pip install -r requirements
```

### Rodar

```
jupyter notebook
```
